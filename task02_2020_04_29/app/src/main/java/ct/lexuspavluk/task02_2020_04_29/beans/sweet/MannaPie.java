package ct.lexuspavluk.task02_2020_04_29.beans.sweet;

public enum MannaPie {
    MANNA_PIE;
    private int nearMetres = 10;

    public boolean mayEat (int distance) {
        return distance < nearMetres;
    }
}
