package ct.lexuspavluk.task02_2020_04_29.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.*;

import java.util.ArrayList;
import java.util.List;

import ct.lexuspavluk.task02_2020_04_29.adapters.AdapterRecycle;
import ct.lexuspavluk.task02_2020_04_29.R;
import ct.lexuspavluk.task02_2020_04_29.beans.DataCard;

import static ct.lexuspavluk.task02_2020_04_29.fragments.FraConst.*;

public class FragmentRecycle extends Fragment {

    private static AdapterRecycle.OnCardClickListener mListener;
    private AdapterRecycle adapterRecycle;

    public static FragmentRecycle newInstance(int cardAmount) {
        FragmentRecycle fragmentRecycle = new FragmentRecycle();

        Bundle args = new Bundle();
        args.putInt(NAME_AMOUNT ,cardAmount);
        fragmentRecycle.setArguments(args);
        return fragmentRecycle;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<DataCard> cardList = null;
        if (getArguments() != null) {
            cardList = fillCardsList(getArguments().getInt(NAME_AMOUNT));
        }
        adapterRecycle = new AdapterRecycle(cardList);
        adapterRecycle.setmListener(mListener);
    }

    @Override
    public View onCreateView( LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recycle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_for_fragment);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapterRecycle);
    }

    private List<DataCard> fillCardsList(int amount) {
        List<DataCard> cardList = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            cardList.add(new DataCard(R.drawable.ic_directions_run_black_24dp,
                    getResources().getString(R.string.title) + " " + i,
                    getResources().getString(R.string.description) + " " + i));
        }
        return cardList;
    }

    public static void setmListener(AdapterRecycle.OnCardClickListener mListener) {
        FragmentRecycle.mListener = mListener;
    }

}

