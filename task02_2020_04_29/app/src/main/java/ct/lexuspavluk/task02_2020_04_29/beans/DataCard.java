package ct.lexuspavluk.task02_2020_04_29.beans;

import android.os.Parcel;
import android.os.Parcelable;

public class DataCard implements Parcelable {

    private int mImage;
    private String mTitle;
    private String mDescription;

    public DataCard(int mImage, String mTitle, String mDescription) {
        this.mImage = mImage;
        this.mTitle = mTitle;
        this.mDescription = mDescription;
    }

    private DataCard(Parcel in) {
        mImage = in.readInt();
        mTitle = in.readString();
        mDescription = in.readString();
    }

    public int getmImage() {
        return mImage;
    }

    public String getmTitle() {
        return mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public static final Creator<DataCard> CREATOR = new Creator<DataCard>() {
        @Override
        public DataCard createFromParcel(Parcel in) {
            return new DataCard(in);
        }

        @Override
        public DataCard[] newArray(int size) {
            return new DataCard[size];
        }
    };

    @Override
    public int describeContents() {
        return mImage;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mImage);
        dest.writeString(mTitle);
        dest.writeString(mDescription);
    }

    @Override
    public String toString() {
        return "DataCard{" +
                "mImage=" + mImage +
                ", mTitle='" + mTitle + '\'' +
                ", mDescription='" + mDescription + '\'' +
                '}';
    }
}
