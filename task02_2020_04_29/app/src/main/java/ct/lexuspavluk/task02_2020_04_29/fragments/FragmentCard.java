package ct.lexuspavluk.task02_2020_04_29.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import ct.lexuspavluk.task02_2020_04_29.beans.DataCard;
import ct.lexuspavluk.task02_2020_04_29.R;

import static ct.lexuspavluk.task02_2020_04_29.fragments.FraConst.LOG_TAG;
import static ct.lexuspavluk.task02_2020_04_29.fragments.FraConst.POSITION;

public class FragmentCard extends Fragment {

    private DataCard mDataCard;

    public static FragmentCard newInstance(DataCard dc) {
        FragmentCard fragmentCard = new FragmentCard();
        Bundle args = new Bundle();
        args.putParcelable(POSITION, dc);
        fragmentCard.setArguments(args);
        return fragmentCard;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDataCard = getArguments().getParcelable(POSITION);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_card, container, false);

        ImageView imageView = view.findViewById(R.id.imageView);
        TextView title = view.findViewById(R.id.textView);
        TextView description = view.findViewById(R.id.textView2);
        Log.d(LOG_TAG, mDataCard.toString());

        imageView.setImageResource(mDataCard.getmImage());
        title.setText(mDataCard.getmTitle());
        description.setText(mDataCard.getmDescription());

        return view;
    }

}
