package ct.lexuspavluk.task02_2020_04_29.fragments;

public class FraConst {
    static final String NAME_AMOUNT = "cardAmount";
    public static final String IMAGE = "image";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    static final String POSITION = "position";
    public final static String LOG_TAG = "myLogs";

}
