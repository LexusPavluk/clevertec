package ct.lexuspavluk.task02_2020_04_29;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ct.lexuspavluk.task02_2020_04_29.adapters.AdapterRecycle;
import ct.lexuspavluk.task02_2020_04_29.beans.DataCard;
import ct.lexuspavluk.task02_2020_04_29.fragments.FragmentCard;
import ct.lexuspavluk.task02_2020_04_29.fragments.FragmentRecycle;

public class MainActivity extends AppCompatActivity implements AdapterRecycle.OnCardClickListener{

    private static final int CARDS_AMOUNT = 1000;

    private FragmentManager fragmentManager;
    private Fragment fragmentRecycle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();
        fragmentRecycle = FragmentRecycle.newInstance(CARDS_AMOUNT);
        FragmentRecycle.setmListener(this);
        if (fragmentManager.findFragmentByTag(FragmentRecycle.class.getSimpleName()) == null) /*getBackStackEntryCount() == 0)*/ {
            getFirstFragment();
        }
    }

    private void getFirstFragment() {
        fragmentManager
                .beginTransaction()
                .add(R.id.fragment_container, fragmentRecycle, FragmentRecycle.class.getSimpleName())
                .commit();
        fragmentManager.executePendingTransactions();
    }

    private void replaceFragment(DataCard dc) {
        Fragment fragmentCard = FragmentCard.newInstance(dc);
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragment_container, fragmentCard)
                .addToBackStack(null)
                .commit();
        fragmentManager.executePendingTransactions();
    }

    @Override
    public void onCardClick(DataCard dc) {
        this.replaceFragment(dc);
    }

}
