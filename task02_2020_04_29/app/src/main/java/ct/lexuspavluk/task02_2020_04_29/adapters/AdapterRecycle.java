package ct.lexuspavluk.task02_2020_04_29.adapters;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ct.lexuspavluk.task02_2020_04_29.R;
import ct.lexuspavluk.task02_2020_04_29.beans.DataCard;

import static ct.lexuspavluk.task02_2020_04_29.fragments.FraConst.LOG_TAG;

public class AdapterRecycle extends RecyclerView.Adapter<AdapterRecycle.ViewHolder> {

    private static OnCardClickListener mListener;
    private List<DataCard> mList = new ArrayList<>();

    public AdapterRecycle(List<DataCard> cards) {
        mList.clear();
        mList = cards;
    }

    public void setmListener(OnCardClickListener mListener) {
        AdapterRecycle.mListener = mListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.bar, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        DataCard dataCard = mList.get(position);
        holder.imageView.setImageResource(dataCard.getmImage());
        holder.titleView.setText(dataCard.getmTitle());
        holder.descriptionView.setText(dataCard.getmDescription());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private AppCompatImageView imageView;
        private TextView titleView;
        private TextView descriptionView;

        ViewHolder(@NonNull final View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            titleView = itemView.findViewById(R.id.titleTextView);
            descriptionView = itemView.findViewById(R.id.descriptionTextView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int imageResource = R.drawable.ic_directions_run_black_24dp;
            String title = this.titleView.getText().toString();
            String description = this.descriptionView.getText().toString();
            DataCard dc = new DataCard(imageResource, title, description);
            Log.d(LOG_TAG, dc.toString());

            mListener.onCardClick(dc);
        }
    }

    public interface OnCardClickListener {
        void onCardClick(DataCard dc);
    }

}
