package ct.lexuspavluk.contacts.util;

import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import ct.lexuspavluk.contacts.R;


public class SharedPrefUtils {
	private static final String SAVED_PHONE = "savedPhone";

	private SharedPreferences sPref;
	private AppCompatActivity activity;


	public SharedPrefUtils(AppCompatActivity activity) {
		this.activity = activity;
	}

	public void savePhoneToSP(String phone) {
		sPref = activity.getPreferences(activity.MODE_PRIVATE);
		SharedPreferences.Editor editor = sPref.edit();
		editor.putString(SAVED_PHONE, phone);
		editor.apply();
	}

	public String loadPhoneFromSP() {
		sPref = activity.getPreferences(activity.MODE_PRIVATE);
		return sPref.getString(SAVED_PHONE, activity.getString(R.string.out_sp_phone));
	}
}
