package ct.lexuspavluk.contacts.fragments;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.app.AlertDialog;

import java.util.Objects;

import ct.lexuspavluk.contacts.R;

public class ContactsDialogFragment extends AppCompatDialogFragment {
    private static PhoneOnClickListener mOnPhoneClick;
    //private final static String KEY_PHONE_LIST = "phoneNumber";
    private static String[] phones;

    public void setmOnPhoneClick(PhoneOnClickListener mListener) {
        ContactsDialogFragment.mOnPhoneClick = mListener;
    }

    public static ContactsDialogFragment newInstance(String[] phoneNumbers) {
        ContactsDialogFragment cdf = new ContactsDialogFragment();
/*
		Bundle args = new Bundle();
		args.putStringArray(KEY_PHONE_LIST, phoneNumbers);
		cdf.setArguments(args);
*/
        phones = phoneNumbers;
        return cdf; // new ContactsDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setItems(phones, (dialog, which) -> mOnPhoneClick.onPhoneClick(phones[which]));
        return builder.create();
    }


    public interface PhoneOnClickListener {
        void onPhoneClick(String string);
    }
}
