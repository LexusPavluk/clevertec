package ct.lexuspavluk.contacts.util;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import java.util.HashMap;

import ct.lexuspavluk.contacts.room.Contact;

public class ContactRecipient extends ContextWrapper {

    private final String TAG = "MyLog";
    private Uri uriContact;
    private String contactID;

    public ContactRecipient(Context base, Uri uriContact) {
        super(base);
        this.uriContact = uriContact;
    }

    public Contact getDtoContact() {
        retrieveContactID();
        Contact c = new Contact();
        c.phoneNumber = retrieveContactNumber();
        HashMap<String, String> names = retrieveName();
        c.firstName = names.get(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME);
        c.lastName = names.get(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME);
        c.email = retrieveEmail();
        return c;
    }

    private void retrieveContactID() {
        String columnName = ContactsContract.Contacts._ID;
        Cursor cursorID = getContentResolver().query(
                uriContact,
                new String[]{columnName},
                null,
                null,
                null);
        if (cursorID != null) {
            if (cursorID.moveToFirst()) {
                int columnIndex = cursorID.getColumnIndex(columnName);
                contactID = cursorID.getString(columnIndex);
            }
            cursorID.close();
        }
        Log.d(TAG, "ContactID = " + contactID);
    }

    private String retrieveContactNumber() {
        String contactNumber = null;
        Cursor cursorNumber = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                        + " = ? AND " + ContactsContract.CommonDataKinds.Phone.TYPE
                        + " = " + ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,
                new String[]{contactID},
                null);
        if (cursorNumber != null) {
            Log.d(TAG, "Rows number ........." + cursorNumber.getCount());
            if (cursorNumber.moveToFirst()) {
                int columnIndex = cursorNumber
                        .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                contactNumber = cursorNumber.getString(columnIndex);
            }
            cursorNumber.close();
        }
        Log.d(TAG, "ContactNumber = " + contactNumber);
        return contactNumber;
    }

    private String retrieveEmail() {
        String contactEmail = null;
        Cursor cursorEmail = getContentResolver().query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                new String[]{contactID},
                null);
        if (cursorEmail != null) {
            if (cursorEmail.moveToFirst()) {
                int columnIndex = cursorEmail
                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);
                contactEmail = cursorEmail.getString(columnIndex);
            }
            cursorEmail.close();
        }
        Log.d(TAG, "contactEmail = " + contactEmail);
        return contactEmail;
    }

    private  HashMap<String, String> retrieveName() {
        String[] projection = new String[]{
                ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
                ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
                ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME};
        String where = ContactsContract.Data.CONTACT_ID + " = ? AND "
                + ContactsContract.Data.MIMETYPE + " = ?";
        String[] wereParams = new String[]{
                contactID,
                ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE};

        Cursor nameCursor = getContentResolver().query(
                ContactsContract.Data.CONTENT_URI,
                projection,
                where,
                wereParams,
                null);

        HashMap<String, String> names = new HashMap<>();
        if (nameCursor != null) {
            Log.d(TAG, "Rows names number ........." + nameCursor.getCount());
            if (nameCursor.moveToFirst()) {
                for (int i = 0; i < projection.length; i++) {
                    int columnIndex = nameCursor.getColumnIndex(projection[i]);
                    names.put(projection[i], nameCursor.getString(columnIndex));
                    Log.d(TAG, "Name" + i + " = " + names.get(projection[i]));
                }
            }
            nameCursor.close();
        }
        return names;
    }

}
