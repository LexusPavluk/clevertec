package ct.lexuspavluk.contacts.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContactDAO {

    @Query(value = "SELECT * FROM Contact")
    LiveData<List<Contact>> getAll();

    @Query(value = "SELECT COUNT(*) FROM Contact")
    int getRowNumber();
    
    @Query(value="SELECT * FROM Contact WHERE phoneNumber = :phone")
    Contact getContactByPhone(String phone);
    
    @Insert
    void insert(Contact contact);

    @Update
    void update(Contact contact);

    @Delete
    void delete(Contact contact);
    
    @Query(value = "DELETE FROM Contact")
    void clearBase();
}
