package ct.lexuspavluk.contacts;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;

import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ct.lexuspavluk.contacts.fragments.ContactsDialogFragment;
import ct.lexuspavluk.contacts.room.App;
import ct.lexuspavluk.contacts.room.AppDatabase;
import ct.lexuspavluk.contacts.room.Contact;
import ct.lexuspavluk.contacts.room.ContactDAO;
import ct.lexuspavluk.contacts.util.ContactRecipient;
import ct.lexuspavluk.contacts.util.NotificationUtils;
import ct.lexuspavluk.contacts.util.SharedPrefUtils;

public class MainActivity extends AppCompatActivity implements ContactsDialogFragment.PhoneOnClickListener {
	private static final int PICK_CONTACT_REQUEST_CODE = 369;
	private static final int RESULT_OK = -1;
	private static final int MY_PERMISSION_READ_CONTACTS = 456;
	private static final int TOAST_MSG_CODE_OUT_RECORD = 123;
	private static final int TOAST_MSG_CODE_RECORD_EXIST = 111;
	private static final int TOAST_MSG_CODE_STORED_TO_DB = 345;
	private static final int TOAST_MSG_CODE_STORED_FAILED = 543;
	private static final int TEXT_VIEW_MSG_CODE_READ_SP = 852;
	private static final String TEXT_VIEW_STATE = "textState";
	
	private final String TAG = "MyLog";
	
	private View rootView;
	private Button selectBtn, contactsBtn, showSpBtn, showNotification;
	private TextView infoTextView;
	
	private Uri uriContact;
	private SharedPrefUtils sPrefUtils;
	
	private Handler mHandler;
	private AppDatabase database = App.getInstance().getDatabase();
	private ContactDAO contactDAO = database.contactDao();
	private Map<String, Contact> contacts = new HashMap<>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		sPrefUtils = new SharedPrefUtils(this);
		
		findViewsByIds();
		setOnClickListeners();
		
		getContactListFromMyDB();
		setMHandler();
	}
	
	private void findViewsByIds() {
		rootView = findViewById(R.id.rootLayout);
		selectBtn = findViewById(R.id.selectButton);
		contactsBtn = findViewById(R.id.contactsButton);
		showSpBtn = findViewById(R.id.showButton);
		infoTextView = findViewById(R.id.infoTextView);
		showNotification = findViewById(R.id.showNotification);
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if(requestCode == PICK_CONTACT_REQUEST_CODE) {
		
		}
	}
	
	@Override
	protected void onSaveInstanceState(@NonNull Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(TEXT_VIEW_STATE, infoTextView.getText().toString());
	}
	
	@Override
	protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		infoTextView.setText(savedInstanceState.getString(TEXT_VIEW_STATE));
	}
	
	private void setMHandler() {
		mHandler = new Handler(Looper.getMainLooper()) {
			@Override
			public void handleMessage(@NonNull Message msg) {
				int msgCode = msg.what;
				switch (msgCode) {
					case TOAST_MSG_CODE_OUT_RECORD:
						showToast(R.string.out_sp_phone);
						break;
					case TOAST_MSG_CODE_RECORD_EXIST:
						showToast(R.string.record_exist);
						break;
					case TOAST_MSG_CODE_STORED_TO_DB:
						showToast(R.string.saving_successfully);
						break;
					case TOAST_MSG_CODE_STORED_FAILED:
						showToast(R.string.saving_failed);
						break;
					case TEXT_VIEW_MSG_CODE_READ_SP:
						//setTextToTextView((String) msg.obj);
						showSnackBar((String) msg.obj);
						break;
				}
			}
		};
	}
	
	private void sendMessage(int code, String text) {
		Message message = mHandler.obtainMessage(code, text);
		message.sendToTarget();
	}
	
	private void getContactListFromMyDB() {
		LiveData<List<Contact>> contactsLD = contactDAO.getAll();
		contactsLD.observe(this, contacts -> {
			for (Contact contact : contacts) {
				this.contacts.put(contact.phoneNumber, contact);
			}
		});
	}
	
	private void showToast(int resource) {
		Toast toast = Toast.makeText(this, resource, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	
	private void showSnackBar(String text) {
		Snackbar snackBar = Snackbar.make(rootView, text, Snackbar.LENGTH_INDEFINITE);
		snackBar.setAction("Ok", v -> snackBar.dismiss());
		snackBar.show();
	}
	
	private void setOnClickListeners() {
		selectBtn.setOnClickListener(v -> new Thread(getContactPick()).start());
		contactsBtn.setOnClickListener(v -> new Thread(getReadNumber()).start());
		showSpBtn.setOnClickListener(v -> new Thread(showStoredNumber()).start());
		showNotification.setOnClickListener(v -> new Thread(showNotification()).start());
	}
	
	private Runnable getContactPick() {
		return () -> {
			if (!checkReadContactPermission(Manifest.permission.READ_CONTACTS)) {
				ActivityCompat.requestPermissions(
						this,
						new String[]{Manifest.permission.READ_CONTACTS},
						MY_PERMISSION_READ_CONTACTS);
			} else {                                                                                    Log.d(TAG, "Permission granted");
				Intent pickContactIntent = new Intent(
						Intent.ACTION_PICK,
						ContactsContract.Contacts.CONTENT_URI);
				startActivityForResult(pickContactIntent,
						PICK_CONTACT_REQUEST_CODE);
			}
		};
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == PICK_CONTACT_REQUEST_CODE &&
				resultCode == MainActivity.RESULT_OK &&
				data != null) {
			uriContact = data.getData();
			new Thread(getInsert()).start();
			Toast.makeText(this, R.string.saving_successfully, Toast.LENGTH_LONG).show();
		}
	}
	
	private Runnable getInsert() {
		return () -> {
			Contact contact = new ContactRecipient(this, uriContact).getDtoContact();
			if (contacts.containsValue(contact)) {
				sendMessage(TOAST_MSG_CODE_RECORD_EXIST, "");
			} else {
				contactDAO.insert(contact);
				if (contactDAO.getContactByPhone(contact.phoneNumber) != null) {
					sendMessage(TOAST_MSG_CODE_STORED_TO_DB, "");
				} else {
					sendMessage(TOAST_MSG_CODE_STORED_FAILED, "");
				}
			}
		};
	}
	
	private Runnable getReadNumber() {
		return () -> {
			if (!checkReadContactPermission(Manifest.permission.READ_CONTACTS)) {
				contactDAO.clearBase();
			}
			if (contacts.size() == 0) {
				sendMessage(TOAST_MSG_CODE_OUT_RECORD, "");
			} else {
				String[] phones = getPhonesFromContacts();
				ContactsDialogFragment dialogFragment = ContactsDialogFragment.newInstance(phones);
				dialogFragment.setmOnPhoneClick(this);
				getSupportFragmentManager()
						.beginTransaction()
						.add(dialogFragment,
								ContactsDialogFragment.class.getSimpleName())
						.commit();
			}
		};
	}
	
	private String[] getPhonesFromContacts() {
		Set<String> phonesSet = contacts.keySet();
		String[] array = new String[phonesSet.size()];
		return phonesSet.toArray(array);
	}
	
	private Runnable showStoredNumber() {
		return () -> {
			String phone = sPrefUtils.loadPhoneFromSP();
			String outRecord = getString(R.string.out_sp_phone);
			if (phone.equals(outRecord)) {
				sendMessage(TOAST_MSG_CODE_OUT_RECORD, "");
			} else {
				sendMessage(TEXT_VIEW_MSG_CODE_READ_SP, phone);
			}
		};
	}
	
	private Runnable showNotification() {
		return () -> {
			String number = sPrefUtils.loadPhoneFromSP();
			if (number != null) {
				NotificationUtils notificationUtils = new NotificationUtils(this);
				notificationUtils.showNotification(contacts.get(number));
			} else {
				sendMessage(TOAST_MSG_CODE_OUT_RECORD, "");
			}
		};
	}
	
	private boolean checkReadContactPermission(String permission) {
		return ContextCompat.checkSelfPermission(this, permission)
				== PackageManager.PERMISSION_GRANTED;
	}
	
	@Override
	public void onPhoneClick(String pickedPhone) {
		sPrefUtils.savePhoneToSP(pickedPhone);
		infoTextView.setText(pickedPhone);
	}
}
