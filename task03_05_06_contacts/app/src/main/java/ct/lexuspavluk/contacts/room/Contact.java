package ct.lexuspavluk.contacts.room;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Objects;

@Entity
public class Contact {
	@PrimaryKey
	@NonNull
	public String phoneNumber;
	@ColumnInfo(name = "first_name")
	public String firstName;
	@ColumnInfo(name = "last_name")
	public String lastName;
	@ColumnInfo(name = "e_mail")
	public String email;
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Contact)) return false;
		Contact contact = (Contact) o;
		return phoneNumber.equals(contact.phoneNumber) &&
				firstName.equals(contact.firstName) &&
				lastName.equals(contact.lastName) &&
				email.equals(contact.email);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(phoneNumber, firstName, lastName, email);
	}
}
