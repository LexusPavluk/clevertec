package ct.lexuspavluk.contacts.util;

import android.app.NotificationManager;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import ct.lexuspavluk.contacts.R;
import ct.lexuspavluk.contacts.room.Contact;

public class NotificationUtils {
	private static final String CHANNEL_ID = "CONTACTS_APP_CHL_ID";
	private static final int NOTIFY_ID = 741;
	private NotificationManager manager;
	private AppCompatActivity activity;
	
	
	public NotificationUtils(AppCompatActivity activity) {
		this.activity = activity;
		manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
	}
	
	public void showNotification(Contact contact) {
		if (manager != null) {
			createNotificationChannel();
			String names = nameLookup(contact.firstName) + " " + nameLookup(contact.lastName);
			NotificationCompat.Style style = new NotificationCompat.BigTextStyle()
					.bigText(names);
			
			NotificationCompat.Builder builder = new NotificationCompat.Builder(activity, CHANNEL_ID)
					.setSmallIcon(R.drawable.ic_launcher_background)
					.setContentTitle(contact.phoneNumber)
					.setAutoCancel(true)
					.setContentText(contact.lastName)
					.setStyle(style)
					.setChannelId(CHANNEL_ID)
					.setContentIntent(createIntent())
					.setPriority(NotificationCompat.PRIORITY_DEFAULT);
			manager.notify(NOTIFY_ID, builder.build());
		}
	}
	
	private String nameLookup(String name) {
		return name != null ? name : "";
	}
	
	private void createNotificationChannel() {
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(
					CHANNEL_ID,
					activity.getString(R.string.app_name),
					NotificationManager.IMPORTANCE_DEFAULT);
			channel.setLightColor(Color.BLUE);
			channel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
			manager.createNotificationChannel(channel);
		}
	}
	
	private PendingIntent createIntent() {
		Intent intent = new Intent(activity, activity.getClass());
		return PendingIntent
				.getActivity(activity, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}
}
