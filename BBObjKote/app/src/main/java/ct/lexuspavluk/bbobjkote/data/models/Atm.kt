package ct.lexuspavluk.bbobjkote.data.models

data class Atm(
    val id: String,
    val area: String,
    val city_type: String,
    val city: String,
    val address_type: String,
    val address: String,
    val house: String,
    val install_place: String,
    val work_time: String,
    val gps_x: Double,
    val gps_y: Double,
    val install_place_full: String, //
    val work_time_full: String,     //
    val ATM_type: String,           //
    val ATM_error: String,          //
    val currency: String,           //
    val cash_in: String,
    val ATM_printer: String         //
): BBankObject