package ct.lexuspavluk.bbobjkote.presenter.utils

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import com.google.android.gms.maps.model.LatLng
import ct.lexuspavluk.bbobjkote.presenter.dto.MapObject
import kotlin.math.acos
import kotlin.math.cos
import kotlin.math.max
import kotlin.math.sin

const val LOG_TAG = "MyTag"

object Earth {
    const val RADIUS = 6371302  //metres
}

fun convert(draw: Drawable): Bitmap {
    if (draw is BitmapDrawable && draw.bitmap != null) return draw.bitmap

    val bitmap =
        Bitmap.createBitmap(
            max(draw.intrinsicWidth, 1),
            max(draw.intrinsicHeight, 1),
            Bitmap.Config.ARGB_8888
        )
    val canvas = Canvas(bitmap)
    draw.setBounds(0, 0, canvas.width, canvas.height)
    draw.draw(canvas)
    return bitmap
}

fun getDistanceBetween(source: LatLng, target: LatLng): Int {
    val sourceLat = Math.toRadians(source.latitude)
    val sourceLong = Math.toRadians(source.longitude)
    val targetLat = Math.toRadians(target.latitude)
    val targetLong = Math.toRadians(target.longitude)
    return (acos(
        sin(sourceLat) * sin(targetLat) +
            cos(sourceLat) * cos(targetLat) *
            cos(sourceLong - targetLong)
    ) * Earth.RADIUS).toInt()
}

fun getDistanceBetween(source: MapObject, target: LatLng): Int {
    return getDistanceBetween(source.position, target)
}

fun getDistanceBetween(source: MapObject, target: MapObject): Int {
    return getDistanceBetween(source, target.position)
}

