package ct.lexuspavluk.bbobjkote.presenter.utils

import ct.lexuspavluk.bbobjkote.R
import ct.lexuspavluk.bbobjkote.domain.dto.BankServiceObject
import ct.lexuspavluk.bbobjkote.domain.dto.ServiceObjectType
import ct.lexuspavluk.bbobjkote.presenter.dto.MapObject

fun map(bankServiceObject: BankServiceObject): MapObject =
    MapObject(
        bankServiceObject.location,
        "${bankServiceObject.name} ${bankServiceObject.installPlace}",
        "${bankServiceObject.addressPrefix} ${bankServiceObject.address}",
        choseMarker(bankServiceObject.type)
    )

private fun choseMarker(objectType: ServiceObjectType): Int =
    when (objectType) {
        ServiceObjectType.ATM -> R.drawable.atm_point
        ServiceObjectType.INFOBOX -> R.drawable.info_point
        ServiceObjectType.FILIALS_INFO -> R.drawable.filial_point
    }
