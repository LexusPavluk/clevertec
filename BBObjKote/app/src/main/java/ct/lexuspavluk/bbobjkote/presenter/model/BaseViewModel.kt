package ct.lexuspavluk.bbobjkote.presenter.model

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel: ViewModel() {

    private val cDisposable = CompositeDisposable()

    protected fun addDisposable(result: Disposable) = cDisposable.add(result)

    override fun onCleared() {
        super.onCleared()
        cDisposable.clear()
    }

}