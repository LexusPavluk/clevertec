package ct.lexuspavluk.bbobjkote.data.models

data class InfoBox(
    val info_id: String,
    val area: String,
    val city_type: String,
    val city: String,
    val address_type: String,
    val address: String,
    val house: String,
    val install_place: String,
    val location_name_desc: String,  //
    val work_time: String,
    val time_long: String,          //
    val gps_x: Double,
    val gps_y: Double,
    val currency: String,           //
    val inf_type: String,           //
    val cash_in_exist: String,      //
    val cash_in: String,
    val type_cash_in: String,       //
    val inf_printer: String,        //
    val region_platej: String,      //
    val popolnenie_platej: String,  //
    val inf_status: String          //
): BBankObject
