package ct.lexuspavluk.bbobjkote.presenter

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

class MapPermissions (private val activity: Activity) {
    companion object {
        private const val PERMISSION_REQUEST_CODE = 111
    }

    fun requestPermissions() =
        ActivityCompat.requestPermissions(
                            activity,
                            arrayOf(ACCESS_COARSE_LOCATION, ACCESS_FINE_LOCATION),
                            PERMISSION_REQUEST_CODE
        )

    fun isLocationPermissionGranted(): Boolean =
        isPermissionAccessCoarseGranted() && isPermissionAccessFineLocationGranted()

    private fun isPermissionAccessCoarseGranted(): Boolean =
        ActivityCompat.checkSelfPermission(activity, ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED

    private fun isPermissionAccessFineLocationGranted(): Boolean =
        ActivityCompat.checkSelfPermission(activity, ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED
}