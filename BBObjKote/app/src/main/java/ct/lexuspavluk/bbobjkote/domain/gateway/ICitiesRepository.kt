package ct.lexuspavluk.bbobjkote.domain.gateway

import com.google.android.gms.maps.model.LatLng

interface ICitiesRepository {

    fun getCities(): Array<String>

    fun getCoordinatesOfCity(city: String): LatLng

}