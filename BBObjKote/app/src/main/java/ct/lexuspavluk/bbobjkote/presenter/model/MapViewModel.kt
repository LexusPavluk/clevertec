package ct.lexuspavluk.bbobjkote.presenter.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import ct.lexuspavluk.bbobjkote.R
import ct.lexuspavluk.bbobjkote.di.DaggerMapComponent
import ct.lexuspavluk.bbobjkote.domain.dto.ServiceObjectType
import ct.lexuspavluk.bbobjkote.domain.gateway.IBankServiceObjectsRepository
import ct.lexuspavluk.bbobjkote.domain.gateway.ICitiesRepository
import ct.lexuspavluk.bbobjkote.presenter.dto.MapObject
import ct.lexuspavluk.bbobjkote.presenter.utils.DistanceToPointComparator
import ct.lexuspavluk.bbobjkote.presenter.utils.getDistanceBetween
import ct.lexuspavluk.bbobjkote.presenter.utils.map
import io.reactivex.Single
import io.reactivex.Single.just
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class MapViewModel : BaseViewModel() {

    companion object{
        private const val OBJECT_QUANTITY = 10
        private const val MIN_DISTANCE = 100
        private val TYPE_QUANTITY = ServiceObjectType.values().size
    }
    private var objectsRepository: IBankServiceObjectsRepository
    private var citiesRepository: ICitiesRepository
    init {
        objectsRepository = DaggerMapComponent.builder().build().getBankServiceObjectRepository()
        citiesRepository = DaggerMapComponent.builder().build().getCitiesRepository()
    }
    private var reasonablyRemoteObjectsList = ArrayList<MapObject>()

    private val cityLiveData = MutableLiveData(citiesRepository.getCities()[0])
    fun getCityLiveData(): LiveData<String> = cityLiveData

    private val nearObjectsMutable = MutableLiveData<List<MapObject>>()
    fun getNearObjectsLiveData(): LiveData<List<MapObject>> = nearObjectsMutable

    private var showProgress = MutableLiveData(false)
    fun isProgress(): LiveData<Boolean> = showProgress

    private var errorLiveData = MutableLiveData<String>()
    fun getErrorLiveData(): LiveData<String> = errorLiveData

    fun getCities(): Array<String> = citiesRepository.getCities()

    fun setCity(chosenCity: String) {
        cityLiveData.postValue(chosenCity)
    }

    fun getCityLocation(city: String): LatLng = citiesRepository.getCoordinatesOfCity(city)

    fun getObjects(city: String) {
        addDisposable(objectsRepository.getBankObjects(city)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .doOnSubscribe { showProgress.value = true
                reasonablyRemoteObjectsList.clear()
            }
            .map { t -> map(t) }
            .sorted(DistanceToPointComparator(getCityLocation(city)))
            .take(OBJECT_QUANTITY * TYPE_QUANTITY.toLong())
            .filter { t: MapObject -> isCloserObject(t) }
            .take(OBJECT_QUANTITY.toLong())
            .mergeWith(getCityCenterObject())
            .toList()
            .subscribe({
                showProgress.postValue(false)
                nearObjectsMutable.postValue(it)},
                {
                showProgress.postValue(false)
                errorLiveData.postValue(it.toString())
                }
            )
        )
    }

    private fun isCloserObject(mapObject: MapObject): Boolean {
        for (oneOfObjects in reasonablyRemoteObjectsList) {
            if (getDistanceBetween(mapObject, oneOfObjects) < MIN_DISTANCE)
                return false
        }
        return reasonablyRemoteObjectsList.add(mapObject)
    }

    private fun getCityCenterObject(): Single<MapObject> =
        just(
            MapObject(
                getCityLocation(cityLiveData.value!!),
                "Центр города $cityLiveData",
                "",
                R.drawable.center_point
            )
        )

}
