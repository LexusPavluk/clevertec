package ct.lexuspavluk.bbobjkote.data.api

import ct.lexuspavluk.bbobjkote.data.models.Atm
import ct.lexuspavluk.bbobjkote.data.models.Filial
import ct.lexuspavluk.bbobjkote.data.models.InfoBox
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BelBankApiService {

    @GET("atm")
    fun getATMs(@Query("city") city: String): Single<List<Atm>>

    @GET("infobox")
    fun getInfoboxes(@Query("city") city: String): Single<List<InfoBox>>

    @GET("filials_info")
    fun getFilials(@Query("city") city: String): Single<List<Filial>>


/*
    companion object Factory {
        private const val BASE_URL = "https://belarusbank.by/api/"

        fun create(): BelBankApiService {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(createOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(
                        GsonBuilder().create())
                )
                .build()
            return retrofit.create(BelBankApiService::class.java)
        }

        private fun createOkHttpClient(): OkHttpClient {
            return OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .addNetworkInterceptor(StethoInterceptor())
                .build()
        }

        private fun getLoggingInterceptor(): HttpLoggingInterceptor {
            return if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            } else {
                HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE)
            }
        }
    }
*/
}