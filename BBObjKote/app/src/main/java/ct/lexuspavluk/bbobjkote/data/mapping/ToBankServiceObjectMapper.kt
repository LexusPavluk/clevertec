package ct.lexuspavluk.bbobjkote.data.mapping

import com.google.android.gms.maps.model.LatLng
import ct.lexuspavluk.bbobjkote.data.models.Atm
import ct.lexuspavluk.bbobjkote.data.models.BBankObject
import ct.lexuspavluk.bbobjkote.data.models.Filial
import ct.lexuspavluk.bbobjkote.data.models.InfoBox
import ct.lexuspavluk.bbobjkote.domain.dto.BankServiceObject
import ct.lexuspavluk.bbobjkote.domain.dto.ServiceObjectType

class ToBankServiceObjectMapper {

    companion object {
/*
        fun <T : BBankObject> mapList (list: List<T>): List<BankServiceObject> =
            list.map { t: T -> map(t) }
*/


        fun <T : BBankObject> map(bBankObject: T): BankServiceObject {
            return when (bBankObject.javaClass.simpleName) {
                "Atm" -> map(
                    bBankObject as Atm
                )
                "InfoBox" -> map(
                    bBankObject as InfoBox
                )
                else -> map(
                    bBankObject as Filial
                )
            }
        }

        private fun map(atm: Atm): BankServiceObject =
            BankServiceObject(
                "ATM # ${atm.id}",
                LatLng(atm.gps_x, atm.gps_y),
                atm.address_type,
                "${atm.address}, ${atm.house}",
                "(${atm.install_place})",
                ServiceObjectType.ATM
            )

        private fun map(infoBox: InfoBox): BankServiceObject =
            BankServiceObject(
                "INFO # ${infoBox.info_id}",
                LatLng(infoBox.gps_x, infoBox.gps_y),
                infoBox.address_type,
                "${infoBox.address}, ${infoBox.house}",
                "(${infoBox.install_place})",
                ServiceObjectType.INFOBOX
            )

        private fun map(filial: Filial): BankServiceObject =
            BankServiceObject(
                "${filial.filial_name}, ${filial.filial_num}",
                LatLng(filial.GPS_X, filial.GPS_Y),
                filial.street_type,
                "${filial.street}, ${filial.home_number}",
                " ",
                ServiceObjectType.FILIALS_INFO
            )
    }
}