package ct.lexuspavluk.bbobjkote.di.modules

import ct.lexuspavluk.bbobjkote.data.api.BelBankApiService
import ct.lexuspavluk.bbobjkote.data.repository.BBankServiceObjectsRepository
import ct.lexuspavluk.bbobjkote.data.repository.CitiesCoordinateRepository
import ct.lexuspavluk.bbobjkote.di.modules.BBankObjApiModule
import ct.lexuspavluk.bbobjkote.domain.gateway.IBankServiceObjectsRepository
import ct.lexuspavluk.bbobjkote.domain.gateway.ICitiesRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [BBankObjApiModule::class])
class RepositoriesModule {

    @Provides
    fun providesBankServiceObjectRepository(apiService: BelBankApiService):
            IBankServiceObjectsRepository = BBankServiceObjectsRepository(apiService)

    @Provides
    fun providesCitiesRepository(): ICitiesRepository = CitiesCoordinateRepository()

}