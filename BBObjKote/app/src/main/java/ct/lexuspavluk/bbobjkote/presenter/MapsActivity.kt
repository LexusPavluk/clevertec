package ct.lexuspavluk.bbobjkote.presenter

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModelProvider
import com.facebook.stetho.Stetho
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import ct.lexuspavluk.bbobjkote.R
import ct.lexuspavluk.bbobjkote.presenter.dto.MapObject
import ct.lexuspavluk.bbobjkote.presenter.model.MapViewModel
import ct.lexuspavluk.bbobjkote.presenter.utils.convert
import kotlinx.android.synthetic.main.activity_maps.*
import java.util.*
import kotlin.properties.Delegates

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var viewModel: MapViewModel
    private lateinit var mMap: GoogleMap
    private var zoom by Delegates.notNull<Float>()
    private val cameraZoom = "cameraZoom"
    private val mapPermissions = MapPermissions(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Stetho.initializeWithDefaults(this)
        setContentView(R.layout.activity_maps)
        viewModel = ViewModelProvider(this).get(MapViewModel::class.java)

        zoom = savedInstanceState?.getFloat(cameraZoom) ?: 13.0f
        getMap()

        setSpinner()
        observeProgressBar()
        observeError()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putFloat(cameraZoom, mMap.cameraPosition.zoom)
    }

    private fun getMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        moveCameraAndDataToCity()
        addObjectMarkersToMap()

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            mapPermissions.requestPermissions()
            return
        }
        mMap.isMyLocationEnabled = mapPermissions.isLocationPermissionGranted()
    }

    private fun moveCameraAndDataToCity() {
        viewModel.getCityLiveData().observe(this, { city ->
            mMap.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    viewModel.getCityLocation(city),
                    zoom
                )
            )
            mMap.clear()
            viewModel.getObjects(city)
        })
    }

    private fun addObjectMarkersToMap() {
        viewModel.getNearObjectsLiveData().observe(
            this,
            { mapObjects ->
                if (mapObjects != null)
                    for (mapObject in mapObjects)
                        addOneMarkerToMap(mapObject)
            })
    }

    private fun addOneMarkerToMap(mapObject: MapObject) {
        mMap.addMarker(
            MarkerOptions()
                .position(mapObject.position)
                .title(mapObject.title)
                .snippet(mapObject.snippet)
                .icon(
                    BitmapDescriptorFactory
                        .fromBitmap(
                            convert(
                                ResourcesCompat.getDrawable(
                                    resources,
                                    mapObject.resourceID,
                                    theme
                                )!!
                            )
                        )
                )
        )
    }

    private fun setSpinner() {
        val adapter = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            viewModel.getCities()
        )
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinner.adapter = adapter
        spinner.setSelection(
            Arrays.binarySearch(
                viewModel.getCities(),
                viewModel.getCityLiveData().value
            )
        )
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?, view: View?, position: Int, id: Long
            ) {
                if (parent != null)
                    viewModel.setCity(parent.getItemAtPosition(position).toString())
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
    }

    private fun observeProgressBar() {
        val progressBar = findViewById<ProgressBar>(R.id.progressCircle)
        viewModel.isProgress().observe(this, {
            progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
    }

    private fun observeError() {
        viewModel.getErrorLiveData().observe(this, { error ->
            Toast.makeText(this, error, Toast.LENGTH_LONG)
                .show()
        })
    }

}