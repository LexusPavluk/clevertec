package ct.lexuspavluk.bbobjkote.presenter.utils

import com.google.android.gms.maps.model.LatLng
import ct.lexuspavluk.bbobjkote.presenter.dto.MapObject

class DistanceToPointComparator(private val point: LatLng): Comparator<MapObject> {
    override fun compare(o1: MapObject?, o2: MapObject?): Int {
        return if(o1 != null && o2 != null) {
             getDistanceBetween(o1, point) -
                     getDistanceBetween(o2, point)
        } else throw IllegalArgumentException("One of arguments is null")
    }
}