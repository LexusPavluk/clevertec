package ct.lexuspavluk.bbobjkote.data.models

data class Filial(
    val id: String,
    val filial_name: String,
    val filial_num: String,
    val cbu_num: String,
    val otd_num: String,
    val name_type: String,
    val name: String,
    val street_type: String,
    val street: String,
    val home_number: String,
    val name_type_prev: String,
    val name_prev: String,
    val street_type_prev: String,
    val street_prev: String,
    val home_number_prev: String,
    val info_text: String,
    val info_worktime: String,
    val info_bank_bik: String,
    val info_bank_unp: String,
    val GPS_X: Double,
    val GPS_Y: Double,
    val bel_number_schet: String,
    val foreign_number_schet: String,
    val phone_info: String
): BBankObject