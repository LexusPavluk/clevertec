package ct.lexuspavluk.bbobjkote.domain.dto

enum class ServiceObjectType {
    ATM, INFOBOX, FILIALS_INFO
}