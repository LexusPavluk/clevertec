package ct.lexuspavluk.bbobjkote.data.repository

import com.google.android.gms.maps.model.LatLng
import ct.lexuspavluk.bbobjkote.domain.gateway.ICitiesRepository
import java.util.*

class CitiesCoordinateRepository : ICitiesRepository {
    private val cities = TreeMap<String, LatLng>()

    init {
        cities["Брест"] = LatLng(52.094034, 23.684942)
        cities["Витебск"] = LatLng(55.183894, 30.204166)
        cities["Гомель"] = LatLng(52.425163, 31.015039)
        cities["Гродно"] = LatLng(53.677849, 23.829477)
        cities["Минск"] = LatLng(53.895921, 27.547567)
        cities["Могилев"] = LatLng(53.908548, 30.342624)
    }

    override fun getCities(): Array<String> {
        val keys = cities.keys.toList()
        return Array(keys.size) { i -> keys.elementAt(i) }
    }


    override fun getCoordinatesOfCity(city: String): LatLng =
        cities[city]!!
}