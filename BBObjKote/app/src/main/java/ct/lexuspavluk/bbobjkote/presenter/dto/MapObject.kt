package ct.lexuspavluk.bbobjkote.presenter.dto

import com.google.android.gms.maps.model.LatLng

data class MapObject (
    val position: LatLng,
    val title: String,
    val snippet: String,
    val resourceID: Int
)