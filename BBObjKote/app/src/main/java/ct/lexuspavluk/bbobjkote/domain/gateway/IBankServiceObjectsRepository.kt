package ct.lexuspavluk.bbobjkote.domain.gateway

import ct.lexuspavluk.bbobjkote.domain.dto.BankServiceObject
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface IBankServiceObjectsRepository {
    fun getBankObjects(city: String): Observable<BankServiceObject>
}