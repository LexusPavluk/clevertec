package ct.lexuspavluk.bbobjkote.data.repository

import ct.lexuspavluk.bbobjkote.data.api.BelBankApiService
import ct.lexuspavluk.bbobjkote.data.mapping.ToBankServiceObjectMapper.Companion.map
import ct.lexuspavluk.bbobjkote.data.models.Atm
import ct.lexuspavluk.bbobjkote.data.models.Filial
import ct.lexuspavluk.bbobjkote.data.models.InfoBox
import ct.lexuspavluk.bbobjkote.domain.dto.BankServiceObject
import ct.lexuspavluk.bbobjkote.domain.gateway.IBankServiceObjectsRepository
import io.reactivex.Observable.concat
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BBankServiceObjectsRepository
@Inject constructor(private var apiService: BelBankApiService) : IBankServiceObjectsRepository {


    override fun getBankObjects(city: String): Observable<BankServiceObject> =
        concat(
            apiService.getInfoboxes(city)
                .flatMapObservable { source: Iterable<InfoBox> -> Observable.fromIterable(source) },
            apiService.getATMs(city)
                .flatMapObservable { source: Iterable<Atm> -> Observable.fromIterable(source) },
            apiService.getFilials(city)
                .flatMapObservable { source: Iterable<Filial> -> Observable.fromIterable(source) })
            .map {t -> map(t)}
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

}