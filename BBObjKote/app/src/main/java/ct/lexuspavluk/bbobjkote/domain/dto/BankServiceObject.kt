package ct.lexuspavluk.bbobjkote.domain.dto

import com.google.android.gms.maps.model.LatLng

data class BankServiceObject(
    val name: String,
    val location: LatLng,
    val addressPrefix: String,
    val address: String,
    val installPlace: String,
    val type: ServiceObjectType
)

