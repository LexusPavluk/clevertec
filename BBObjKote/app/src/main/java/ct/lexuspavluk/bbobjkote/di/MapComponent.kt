package ct.lexuspavluk.bbobjkote.di

import ct.lexuspavluk.bbobjkote.di.modules.BBankObjApiModule
import ct.lexuspavluk.bbobjkote.di.modules.OkHttpClientModule
import ct.lexuspavluk.bbobjkote.di.modules.RepositoriesModule
import ct.lexuspavluk.bbobjkote.domain.gateway.IBankServiceObjectsRepository
import ct.lexuspavluk.bbobjkote.domain.gateway.ICitiesRepository
import dagger.Component

@Component(modules = [
    RepositoriesModule::class,
    BBankObjApiModule::class,
    OkHttpClientModule::class
])
interface MapComponent {

    fun getBankServiceObjectRepository() : IBankServiceObjectsRepository

    fun getCitiesRepository() : ICitiesRepository

}