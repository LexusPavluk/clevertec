package ct.lexuspavluk.mapapprx.domain;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

import ct.lexuspavluk.mapapprx.data.ToBbObjectMapper;
import ct.lexuspavluk.mapapprx.data.api.BBObjectApiService;
import ct.lexuspavluk.mapapprx.data.service.RetrofitService;
import ct.lexuspavluk.mapapprx.domain.dto.BBankObject;

public class ObjectLocationRequesterRX {
	public static final String CITY = "Гомель";

	private BBObjectApiService mBbObjectApiService;

	public ObjectLocationRequesterRX() {
		createLocationApiService();
	}
	
	private void createLocationApiService() {
		Retrofit retrofit = RetrofitService
									.getInstance()
									.getRetrofit();
		mBbObjectApiService = retrofit.create(BBObjectApiService.class);
	}

	public Observable<BBankObject> getBBankObjectsInfo() {
		return Observable.concat(
				mBbObjectApiService.getATMs(CITY).flatMapObservable(Observable::fromIterable),
				mBbObjectApiService.getInfoboxes(CITY).flatMapObservable(Observable::fromIterable),
				mBbObjectApiService.getFilials(CITY).flatMapObservable(Observable::fromIterable)
		)
				.map(ToBbObjectMapper::map)
				.sorted(new LocationObjComparator())
				.distinct(BBankObject::getAddress)
				.take(10)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread());
	}

}
