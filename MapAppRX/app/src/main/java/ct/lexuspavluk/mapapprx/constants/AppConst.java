package ct.lexuspavluk.mapapprx.constants;

import com.google.android.gms.maps.model.LatLng;

public class AppConst {

	public static final LatLng GOMEL = new LatLng(52.425163d, 31.015039d);

	public static final String TAG = "MyTag";

}
