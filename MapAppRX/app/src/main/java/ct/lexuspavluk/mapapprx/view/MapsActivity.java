package ct.lexuspavluk.mapapprx.view;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

import com.facebook.stetho.Stetho;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

import ct.lexuspavluk.mapapprx.R;
import ct.lexuspavluk.mapapprx.domain.TypeObject;
import ct.lexuspavluk.mapapprx.domain.dto.BBankObject;
import ct.lexuspavluk.mapapprx.domain.ObjectLocationRequesterRX;
import ct.lexuspavluk.mapapprx.constants.AppConst;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private static final int PERMISSIONS_REQUEST_CODE = 111;
    private static final float START_LOCATION_ZOOM = 12f;

    private GoogleMap mMap;
    private Bitmap markerATM;
    private Bitmap markerInfo;
    private Bitmap markerFilial;
    private Bitmap markerCenter;
    private float zoom;

    private ObjectLocationRequesterRX mLocationRequester = new ObjectLocationRequesterRX();
    private CompositeDisposable mDisposable = new CompositeDisposable();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);

        setContentView(R.layout.activity_maps);
        setMarkers();
        zoom = savedInstanceState != null ?
                savedInstanceState.getFloat("cameraZoom") :
                START_LOCATION_ZOOM;
    
        getMap();
    }

    private void getMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat("cameraZoom", mMap.getCameraPosition().zoom);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDisposable.dispose();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(AppConst.GOMEL, zoom));
        mMap.addMarker(new MarkerOptions()
                .position(AppConst.GOMEL)
                .icon(BitmapDescriptorFactory.fromBitmap(markerCenter)));

        mDisposable.add(mLocationRequester.getBBankObjectsInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<BBankObject>() {
                    @Override
                    public void onNext(BBankObject bBankObject) {
                        addMapMarker(bBankObject);
                    }
                
                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                
                    @Override
                    public void onComplete() {
                    
                    }
                })
        );
    
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestMapPermissions();
            return;
        }
        
        mMap.setMyLocationEnabled(isLocationPermissionsGranted());
    }

    private void addMapMarker(BBankObject bBankObject) {
        Log.d(AppConst.TAG, bBankObject.toString());
        Bitmap currentMarker = choseMarker(bBankObject.getType());
        mMap.addMarker(new MarkerOptions()
                .position(bBankObject.getLatLng())
                .title(bBankObject.getName() + " " + bBankObject.getInstallPlace())
                .snippet(bBankObject.getAddressType() + bBankObject.getAddress())
                .icon(BitmapDescriptorFactory.fromBitmap(currentMarker)));
    }
    
    private Bitmap choseMarker(TypeObject type) {
        switch (type) {
            case INFOBOX: return markerInfo;
            case FILIALS_INFO: return markerFilial;
            default: return markerATM;
        }
    }

    private void setMarkers() {
        markerATM = setBitmapMarker(R.drawable.atm_point);
        markerInfo = setBitmapMarker(R.drawable.info_point);
        markerFilial = setBitmapMarker(R.drawable.filial_point);
        markerCenter = setBitmapMarker(R.drawable.center_point);
    }
    
    private Bitmap setBitmapMarker(int idRes) {
        return BitmatFromDrawable.convert(
                ResourcesCompat.getDrawable(getResources(),
                        idRes, getTheme()));
    }


    private boolean isLocationPermissionsGranted() {
        return isPermissionAccessCoarseApproved() && isPermissionAccessFineLocationApproved();
    }

    private boolean isPermissionAccessCoarseApproved() {
        return ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private boolean isPermissionAccessFineLocationApproved() {
        return ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED;
    }

    private void requestMapPermissions() {
        ActivityCompat
                .requestPermissions(
                        this,
                        new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                        PERMISSIONS_REQUEST_CODE);
    }

}