package ct.lexuspavluk.mapapprx.data.dto;

import com.google.gson.annotations.SerializedName;

public class Atm {

    @SerializedName("id")
    private String id;
    /*
        @SerializedName("area")
        private String area;

        @SerializedName("city_type")
        private String cityType;

        @SerializedName("city")
        private String city;
    */
    @SerializedName("address_type")
    private String addressType;

    @SerializedName("address")
    private String address;

    @SerializedName("house")
    private String house;

	@SerializedName("install_place")
	private String installPlace;

/*
	@SerializedName("work_time")
	private String workTime;

*/
    @SerializedName("gps_x")
    private double gpsX;

    @SerializedName("gps_y")
    private double gpsY;
/*

 	@SerializedName("install_place_full")
	private String installPlaceFull;

	@SerializedName("work_time_full ")
	private String workTimeFull;

	@SerializedName("ATM_type")
	private String atmType;

	@SerializedName("ATM_error")
	private String atmError;

	@SerializedName("currency")
	private String currency;

	@SerializedName("cash_in")
	private String cashIn;

	@SerializedName("ATM_printer")
	private String atmPrinter;
*/


    public String getId() {
        return id;
    }

    public String getAddressType() {
        return addressType;
    }

    public String getAddress() {
        return address;
    }

    public String getHouse() {
        return house;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public double getGpsX() {
		return gpsX;
	}

	public double getGpsY() {
		return gpsY;
	}

}
