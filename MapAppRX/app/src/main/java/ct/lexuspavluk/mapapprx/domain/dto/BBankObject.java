package ct.lexuspavluk.mapapprx.domain.dto;

import com.google.android.gms.maps.model.LatLng;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import ct.lexuspavluk.mapapprx.domain.TypeObject;

public class BBankObject {
	private String name;
	private LatLng latLng;
	private String addressType;
	private String address;
	private String installPlace;
	private TypeObject type;
	
	public BBankObject(String name,
					   LatLng latLng,
					   String addressType,
					   String address,
					   String installPlace,
					   TypeObject type) {
		this.name = name;
		this.latLng = latLng;
		this.addressType = addressType;
		this.address = address;
		this.installPlace = installPlace;
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	
	public LatLng getLatLng() {
		return latLng;
	}
	
	public String getAddressType() {
		return addressType;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getInstallPlace() {
		return installPlace;
	}
	
	public TypeObject getType() {
		return type;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BBankObject)) return false;
		BBankObject that = (BBankObject) o;
		return name.equals(that.name) &&
				latLng.equals(that.latLng) &&
				address.equals(that.address) &&
				installPlace.equals(that.installPlace) &&
				type == that.type;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name, latLng, address, installPlace, type);
	}
	
	@NotNull
	@Override
	public String toString() {
		return "Object{" +
				"name='" + name + '\'' +
				", latLng=" + latLng +
				'}';
	}
}
