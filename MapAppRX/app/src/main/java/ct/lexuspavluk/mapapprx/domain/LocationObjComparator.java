package ct.lexuspavluk.mapapprx.domain;

import java.util.Comparator;

import ct.lexuspavluk.mapapprx.domain.dto.BBankObject;
import ct.lexuspavluk.mapapprx.constants.AppConst;

public class LocationObjComparator implements Comparator<BBankObject> {
	@Override
	public int compare(BBankObject o1, BBankObject o2) {
		return DistanceToTarget.getDistance(o1, AppConst.GOMEL) -
				DistanceToTarget.getDistance(o2, AppConst.GOMEL);
	}
}
