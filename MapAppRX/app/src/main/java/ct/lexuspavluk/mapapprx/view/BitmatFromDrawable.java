package ct.lexuspavluk.mapapprx.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class BitmatFromDrawable {

	private BitmatFromDrawable() {
	}

	public static Bitmap convert (Drawable draw) {
		Bitmap bitmap = null;
		
		if (draw instanceof BitmapDrawable) {
			BitmapDrawable bitmapDrawable = (BitmapDrawable) draw;
			if(bitmapDrawable.getBitmap() != null) {
				return bitmapDrawable.getBitmap();
			}
		}
		
		if(draw.getIntrinsicWidth() <= 0 || draw.getIntrinsicHeight() <= 0) {
			bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
		} else {
			bitmap = Bitmap.createBitmap(
					draw.getIntrinsicWidth(),
					draw.getIntrinsicHeight(),
					Bitmap.Config.ARGB_8888);
		}
		
		Canvas canvas = new Canvas(bitmap);
		draw.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		draw.draw(canvas);
		return bitmap;
	}
}
