package ct.lexuspavluk.mapapprx.data.service;

import com.facebook.stetho.BuildConfig;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import com.google.gson.GsonBuilder;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    public static final String BASE_URL = "https://belarusbank.by/api/";

    private static RetrofitService retrofitInstance;
    private Retrofit mRetrofit;

    private RetrofitService() {
        createRetrofit();
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public static RetrofitService getInstance() {
       if (retrofitInstance == null) {
           retrofitInstance = new RetrofitService();
       }
       return retrofitInstance;
    }

    private void createRetrofit() {
         mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(createOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.
                                create(new GsonBuilder().create()))
                .build();
    }

    private OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        if (BuildConfig.DEBUG) {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
        }
    }

}
