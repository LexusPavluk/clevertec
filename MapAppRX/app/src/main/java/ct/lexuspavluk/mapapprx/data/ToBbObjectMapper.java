package ct.lexuspavluk.mapapprx.data;

import com.google.android.gms.maps.model.LatLng;

import ct.lexuspavluk.mapapprx.data.dto.Atm;
import ct.lexuspavluk.mapapprx.data.dto.Filial;
import ct.lexuspavluk.mapapprx.data.dto.Infobox;
import ct.lexuspavluk.mapapprx.domain.TypeObject;
import ct.lexuspavluk.mapapprx.domain.dto.BBankObject;

public class ToBbObjectMapper {

	private ToBbObjectMapper() {
	}

	public static BBankObject map(Object bbObject) {
		TypeObject typeObject = TypeObject.valueOf(bbObject.getClass().getSimpleName());
		switch (typeObject) {
			case ATM: return map(bbObject);
			case INFOBOX: return map(bbObject);
			case FILIALS_INFO: return map(bbObject);
			default: throw new ClassCastException("Cast to bbObject error");
		}
	}
	
	private static BBankObject map(Atm atm) {
		return new BBankObject(
				"ATM #" + atm.getId(),
				new LatLng(atm.getGpsX(), atm.getGpsY()),
				atm.getAddressType(),
				setAddress(atm.getAddress(), atm.getHouse()),
				"(" + atm.getInstallPlace() + ")",
				TypeObject.ATM);
	}
	
	private static BBankObject map(Filial filial) {
		return new BBankObject(
				filial.getFilialName(),
				new LatLng(filial.getGpsX(), filial.getGpsY()),
				filial.getAddressType(),
				setAddress(filial.getAddress(),filial.getHouse()),
				"",
				TypeObject.FILIALS_INFO);
	}
	
	private static BBankObject map(Infobox infobox) {
		return new BBankObject(
				"INFO #" + infobox.getId(),
				new LatLng(infobox.getGpsX(), infobox.getGpsY()),
				infobox.getAddressType(),
				setAddress(infobox.getAddress(),infobox.getHouse()),
				"(" + infobox.getInstallPlace() + ")",
				TypeObject.INFOBOX);
	}
	
	private static String setAddress(String adr, String house) {
		return adr + ", " + house;
	}
	
}
