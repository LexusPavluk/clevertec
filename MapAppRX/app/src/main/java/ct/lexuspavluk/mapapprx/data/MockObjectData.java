package ct.lexuspavluk.mapapprx.data;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import ct.lexuspavluk.mapapprx.constants.AppConst;
import ct.lexuspavluk.mapapprx.data.dto.Filial;

public class MockObjectData {
	private static final int COUNT = 512;
	private final double dX = 6.0;
	private final double dY = 10.0;

	
	public Single<List<Filial>> getMosk() {
		List<Filial> list = new ArrayList<>();
		for (int i = 0; i < COUNT; i++) {
			for (int j = 0; j < COUNT; j++) {
				Filial fil = new Filial();
				LatLng point = getCoordinate(i, j);
//				fil.setFilialName(i + j + "");
//				fil.setAddressType("");
//				fil.setAddress("" + i + i + i);
//				fil.setHouse("" + j);
//				fil.setGpsX(point.latitude);
//				fil.setGpsY(point.longitude);
				list.add(fil);
			}
		}
		return Single.just(list);
	}
	
	private LatLng getCoordinate (int i, int j) {
		double latt =  Math.round((AppConst.GOMEL.latitude - dX / 2 + dX / COUNT * i) * 1000_000) / 1000_000.0;
		double longt =  Math.round((AppConst.GOMEL.longitude - dY / 2 + dY / COUNT * j) * 1000_000) / 1000_000.0;
		return new LatLng(latt, longt);
	}
	
}
