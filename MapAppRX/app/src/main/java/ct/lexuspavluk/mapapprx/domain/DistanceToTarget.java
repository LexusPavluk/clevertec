package ct.lexuspavluk.mapapprx.domain;

import com.google.android.gms.maps.model.LatLng;

import ct.lexuspavluk.mapapprx.domain.dto.BBankObject;
import ct.lexuspavluk.mapapprx.constants.AppConst;

public class DistanceToTarget {
	private static final int EARTH_RADIUS = 6371302;

	private DistanceToTarget() {
	}

	public static int getDistance(BBankObject bBankObject, LatLng target) {
		LatLng bbLatLng = bBankObject.getLatLng();
		return getDistance(bbLatLng, target);
	}
	
	public static int getDistance(LatLng source, LatLng target) {
		double dLat = target.latitude - source.latitude;
		double dLng = target.longitude - source.longitude;
		double angle = Math.sqrt(Math.pow(dLat, 2) + Math.pow(dLng / 1.7, 2));
		return (int) (Math.tan(Math.toRadians(angle)) * EARTH_RADIUS);
	}
	
}
