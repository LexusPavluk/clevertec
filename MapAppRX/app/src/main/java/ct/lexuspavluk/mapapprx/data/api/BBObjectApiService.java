package ct.lexuspavluk.mapapprx.data.api;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

import ct.lexuspavluk.mapapprx.data.dto.Atm;
import ct.lexuspavluk.mapapprx.data.dto.Filial;
import ct.lexuspavluk.mapapprx.data.dto.Infobox;

public interface BBObjectApiService {

    @GET("atm")
    Single<List<Atm>> getATMs(
            @Query("city") String city);

    @GET("infobox")
    Single<List<Infobox>> getInfoboxes(
            @Query("city") String city);

    @GET("filials_info")
    Single<List<Filial>> getFilials(
            @Query("city") String city);
    
}
