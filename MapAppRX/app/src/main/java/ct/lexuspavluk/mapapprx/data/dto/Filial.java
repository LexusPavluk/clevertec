package ct.lexuspavluk.mapapprx.data.dto;

import com.google.gson.annotations.SerializedName;

public class Filial {

/*
	@SerializedName("id")
	private String id;
*/

	@SerializedName("filial_name")
	private String filialName ;

	/*
        @SerializedName("filial_num")
        private String filialNum ;

        @SerializedName("cbu_num")
        private String cbuNum ;

        @SerializedName("otd_num")
        private String otdNum ;

        @SerializedName("name_type")
        private String cityType;

        @SerializedName("name")
        private String city;
    */
	@SerializedName("street_type")
	private String addressType;

	@SerializedName("street")
	private String address;

	@SerializedName("home_number")
	private String house;

/*
	@SerializedName("info_text")
	private String infoText ;

	@SerializedName("info_worktime")
	private String workTime;

	@SerializedName("info_bank_bik")
	private String infoBik;

	@SerializedName("info_bank_unp")
	private String infoUnp;

*/
	@SerializedName("GPS_X")
	private double gpsX;

	@SerializedName("GPS_Y")
	private double gpsY;

/*
	@SerializedName("bel_number_schet")
	private String belNumberSchet;

	@SerializedName("foreign_number_schet")
	private String foreignNumberSchet;

	@SerializedName("phone_info")
	private String phoneInfo;

*/
	
	public String getFilialName() {
		return filialName;
	}
	
	public String getAddressType() {
		return addressType;
	}

	public String getAddress() {
		return address;
	}

	public String getHouse() {
		return house;
	}

	public double getGpsX() {
		return gpsX;
	}

	public double getGpsY() {
		return gpsY;
	}
	
}
