package ct.lexuspavluk.mapapprx.domain;

import ct.lexuspavluk.mapapprx.data.dto.Atm;
import ct.lexuspavluk.mapapprx.data.dto.Filial;
import ct.lexuspavluk.mapapprx.data.dto.Infobox;

public enum TypeObject {
	ATM(Atm.class.getSimpleName()),
	INFOBOX(Infobox.class.getSimpleName()),
	FILIALS_INFO(Filial.class.getSimpleName());

	private String className;

	TypeObject(String simpleName) {
		this.className = simpleName;
	}

	public String getClassName() {
		return className;
	}
}
