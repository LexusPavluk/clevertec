package ct.lexuspavluk.mapapprx.data.dto;

import com.google.gson.annotations.SerializedName;

public class Infobox {
	
	@SerializedName("info_id")
	private String id;
	
	/*
        @SerializedName("area")
        private String area;

        @SerializedName("city_type")
        private String cityType;

        @SerializedName("city")
        private String city;
    */
	@SerializedName("address_type")
	private String addressType;

	@SerializedName("address")
	private String address;

	@SerializedName("house")
	private String house;

	@SerializedName("install_place")
	private String installPlace;

	/*
        @SerializedName("location_name_desc")
        private String locationNameDesc;

        @SerializedName("work_time")
        private String workTime;

        @SerializedName("time_long")
        private String timeLong;

    */
	@SerializedName("gps_x")
	private double gpsX;

	@SerializedName("gps_y")
	private double gpsY;

/*
	@SerializedName("currency")
	private String currency;

	@SerializedName("inf_type")
	private String infType;

	@SerializedName("cash_in_exist")
	private String cashInExist;

	@SerializedName("cash_in")
	private String cashIn;

	@SerializedName("type_cash_in")
	private String typeCashIn;

	@SerializedName("inf_printer")
	private String infPrinter;

	@SerializedName("region_platej")
	private String regionPlatej;

	@SerializedName("popolnenie_platej")
	private String popolneniePlatej;

	@SerializedName("infStatus ")
	private String infStatus ;
*/
	
	public String getId() {
		return id;
	}
	
	public String getAddressType() {
		return addressType;
	}

	public String getAddress() {
		return address;
	}

	public String getHouse() {
		return house;
	}

	public String getInstallPlace() {
		return installPlace;
	}

	public double getGpsX() {
		return gpsX;
	}

	public double getGpsY() {
		return gpsY;
	}


}
