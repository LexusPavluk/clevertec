package ct.lexuspavluk.mapapp;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ct.lexuspavluk.mapapp.models.AtmModel;
import ct.lexuspavluk.mapapp.services.AtmLocationRequester;
import ct.lexuspavluk.mapapp.utils.AppConst;
import ct.lexuspavluk.mapapp.utils.BitmatFromDrawable;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
	
	private static final int PERMISSIONS_REQUEST_CODE = 111;
	private static final int CURRENT_LOCATION_ZOOM = 11;
	
	private GoogleMap mMap;
	private Bitmap marker;
	
	private AtmLocationRequester requester = new AtmLocationRequester();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_maps);
		
		getAvailableMap();
	}
	
	
	private void getMap() {
		SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
				.findFragmentById(R.id.map);
		if (mapFragment != null) {
			mapFragment.getMapAsync(this);
		}
	}
	
	/**
	 * Manipulates the map once available.
	 * This callback is triggered when the map is ready to be used.
	 * This is where we can add markers or lines, add listeners or move the camera.
	 */
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		setBitmapMarker();
		requester.loadAtmInfo();
		addMapMarkers();
		
		mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(AppConst.GOMEL, CURRENT_LOCATION_ZOOM));
		if (ActivityCompat.checkSelfPermission(
				this, Manifest.permission.ACCESS_FINE_LOCATION) !=
				PackageManager.PERMISSION_GRANTED &&
				ActivityCompat.checkSelfPermission(
						this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
						PackageManager.PERMISSION_GRANTED) {
			requestMapPermissions();
		}
		mMap.setMyLocationEnabled(true);
	}
	
	private void addMapMarkers() {
		requester.getLocationMutableLiveData().observe(this, atmModels -> {
			for (AtmModel model: atmModels) {
				addMapMarker(model);
			}
		});
	}
	
	private void addMapMarker(AtmModel atm) {
		LatLng latLng = new LatLng(atm.getGpsX(), atm.getGpsY());
		String address = atm.getAddressType()
				+ atm.getAddress() + ", "
				+ atm.getHouse();
		
		float alfa = (float) getAlfaByDistance(latLng);
		mMap.addMarker(new MarkerOptions()
						.position(latLng)
						.title("#" + atm.getIdATM())
						.snippet(address)
				.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
				.icon(BitmapDescriptorFactory.fromBitmap(marker))
				.alpha(alfa));
	}
	
	private double getAlfaByDistance(LatLng latLng) {
		double sharpRoundDistance = 1500.0;
		double minAlfavalue = 0.2;
		return Math.min(1.0, (minAlfavalue + sharpRoundDistance / Math.max(getDistanceToATM(latLng), 1)));
	}
	
	private double getDistanceToATM(LatLng latLng) {
		double dLat = latLng.latitude - AppConst.GOMEL.latitude;
		double dLng = latLng.longitude - AppConst.GOMEL.longitude;
		double angle = Math.sqrt(Math.pow(dLat,2) + Math.pow(dLng,2));
		return  Math.tan(Math.toRadians(angle)) * AppConst.EARTH_RADIUS;
	}
	
	
	private void setBitmapMarker() {
		marker = BitmatFromDrawable.convert(
				ResourcesCompat.getDrawable(getResources(),
						R.drawable.atm_point, getTheme()));
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == PERMISSIONS_REQUEST_CODE) {
			getAvailableMap();
		}
	}
	
	private void getAvailableMap() {
		if (isPermissionAccessCoarseLocationApproved() && isPermissionAccessFineLocationApproved()) {
			getMap();
		} else {
			requestMapPermissions();
		}
	}
	
	private boolean isPermissionAccessCoarseLocationApproved() {
		return ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) ==
				PackageManager.PERMISSION_GRANTED;
	}
	
	private boolean isPermissionAccessFineLocationApproved() {
		return ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) ==
				PackageManager.PERMISSION_GRANTED;
	}
	
	
	private void requestMapPermissions() {
		ActivityCompat
				.requestPermissions(
						this,
						new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
						PERMISSIONS_REQUEST_CODE);
	}
	
	
}