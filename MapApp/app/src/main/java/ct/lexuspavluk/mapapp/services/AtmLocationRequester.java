package ct.lexuspavluk.mapapp.services;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.facebook.stetho.BuildConfig;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import ct.lexuspavluk.mapapp.api.LocObjApiService;
import ct.lexuspavluk.mapapp.models.AtmModel;
import ct.lexuspavluk.mapapp.utils.AppConst;
import ct.lexuspavluk.mapapp.utils.TypeObject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AtmLocationRequester {
	private LocObjApiService locObjApiService;
	private static final String BASE_URL = "https://belarusbank.by/api/";
	//private String[] currencyS = {"BYN"/*, "USD", "EUR"*/};
	private MutableLiveData<List<AtmModel>> locationMutableLiveData = new MutableLiveData<>();
	
	public AtmLocationRequester() {
		createLocationApiService();
	}
	
	public MutableLiveData<List<AtmModel>> getLocationMutableLiveData() {
		return locationMutableLiveData;
	}
	
	public void loadAtmInfo() {
		locObjApiService
				.getAtmS(TypeObject.INFOBOX.toString().toLowerCase(), AppConst.CITY)
				.enqueue(new Callback<List<AtmModel>>() {
					@Override
					public void onResponse(
							@NotNull Call<List<AtmModel>> call,
							@NotNull Response<List<AtmModel>> response) {
						if (response.body() != null) {
							locationMutableLiveData.setValue((response.body()));
						}
					}
					
					@Override
					public void onFailure(@NotNull Call<List<AtmModel>> call,
										  @NotNull Throwable t) {
						Log.d(AppConst.TAG, "All Gone!!!");
					}
				});
	}
	
	private void createLocationApiService() {
		Retrofit retrofit = new Retrofit.Builder()
				.baseUrl(BASE_URL)
				.client(createOkHttpClient())
				.addConverterFactory(
						GsonConverterFactory.
								create(new GsonBuilder().create()))
				.build();
		
		locObjApiService = retrofit.create(LocObjApiService.class);
	}

	private OkHttpClient createOkHttpClient() {
		return new OkHttpClient.Builder()
				.addInterceptor(getLoggingInterceptor())
				.addNetworkInterceptor(new StethoInterceptor())
				.build();
	}

	private HttpLoggingInterceptor getLoggingInterceptor() {
		if (BuildConfig.DEBUG) {
			return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
		} else {
			return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.NONE);
		}
	}

}
