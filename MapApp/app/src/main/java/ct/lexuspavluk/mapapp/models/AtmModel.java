package ct.lexuspavluk.mapapp.models;

import com.google.gson.annotations.SerializedName;

public class AtmModel {
	
	@SerializedName("id")
	private String idATM;
	
	@SerializedName("city")
	private String city;
	
	@SerializedName("address_type")
	private String addressType;
	
	@SerializedName("address")
	private String address;
	
	@SerializedName("house")
	private String house;
	
	@SerializedName("gps_x")
	private double gpsX;
	
	@SerializedName("gps_y")
	private double gpsY;
	
	@SerializedName("currency")
	private String currency;
	
	
	public String getIdATM() {
		return idATM;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getAddressType() {
		return addressType;
	}
	
	public String getAddress() {
		return address;
	}
	
	public String getHouse() {
		return house;
	}
	
	public double getGpsX() {
		return gpsX;
	}
	
	public double getGpsY() {
		return gpsY;
	}
	
	public String getCurrency() {
		return currency;
	}

	@Override
	public String toString() {
		return "AtmModel{" +
				"idATM='" + idATM + '\'' +
				", city='" + city + '\'' +
				", addressType='" + addressType + '\'' +
				", address='" + address + '\'' +
				", house='" + house + '\'' +
				", gpsX=" + gpsX +
				", gpsY=" + gpsY +
				", currency='" + currency + '\'' +
				'}';
	}
}
