package ct.lexuspavluk.mapapp.api;


import java.util.List;

import ct.lexuspavluk.mapapp.models.AtmModel;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface LocObjApiService {

    @GET("{objectType}")
    Call<List<AtmModel>> getAtmS(
            @Path("objectType") String objectType,
			@Query("city") String city);
    
}
