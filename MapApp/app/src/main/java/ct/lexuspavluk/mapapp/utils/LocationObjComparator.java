package ct.lexuspavluk.mapapp.utils;

import com.google.android.gms.maps.model.LatLng;

import java.util.Comparator;

import ct.lexuspavluk.mapapp.models.AtmModel;

public class LocationObjComparator implements Comparator<AtmModel> {
	@Override
	public int compare(AtmModel o1, AtmModel o2) {
		return getDistanceToATM(o1) - getDistanceToATM(o2);
	}
	
	private int getDistanceToATM(AtmModel atmModel) {
		LatLng latLng = new LatLng(atmModel.getGpsX(), atmModel.getGpsY());
		double dLat = latLng.latitude - AppConst.GOMEL.latitude;
		double dLng = latLng.longitude - AppConst.GOMEL.longitude;
		double angle = Math.sqrt(Math.pow(dLat,2) + Math.pow(dLng,2));
		return (int) (Math.tan(Math.toRadians(angle)) * AppConst.EARTH_RADIUS);
	}
	
}
